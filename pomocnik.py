#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Work with Python 3.6
import discord
from time import sleep
import time
import json
import datetime
import re
from colorama import init, Fore, Back, Style


regexpsc = r"([0-9]{4}(\-|)[0-9]{4}(\-|)[0-9]{4}(\-|)[0-9]{4})"
#psc regex ([0-9]{4}(\-|)[0-9]{4}(\-|)[0-9]{4}(\-|)[0-9]{4})

init(convert=True)

print(Style.BRIGHT+Fore.RED + 'Uruchamianie...'+Style.RESET_ALL)

TOKEN = 'MzMwMDc3NzM0MjA0MDgwMTI4.XLITNQ.-KXulP3FsaJLdWGiSE7eP1oONxw'
client = discord.Client()

def colorServer(text):
    return Style.BRIGHT+Fore.RED + str(text) +Style.RESET_ALL

def colorUser(text):
    return Style.BRIGHT+Fore.CYAN + str(text) +Style.RESET_ALL

def hightextconwert(tekst):
    tekst = tekst.lower()
    out = ''
    for c in tekst:
        if c == ' ':
            emoji = '   ' # spacja 3x
        elif c == '0':
            emoji = ':zero:'
        elif c == '1':
            emoji = ':one:'
        elif c == '2':
            emoji = ':two:'
        elif c == '3':
            emoji = ':three:'
        elif c == '4':
            emoji = ':four:'
        elif c == '5':
            emoji = ':five:'
        elif c == '6':
            emoji = ':six:'
        elif c == '7':
            emoji = ':seven:'
        elif c == '8':
            emoji = ':eight:'
        elif c == '9':
            emoji = ':nine:'
        else:
            emoji = ":regional_indicator_"+pl2en(c)+":"
        out = out + " " +emoji
    return out

def pl2en(txt):
    txt = txt.replace("ą", "a")
    txt = txt.replace("ć", "c")
    txt = txt.replace("ę", "e")
    txt = txt.replace("ł", "l")
    txt = txt.replace("ń", "n")
    txt = txt.replace("ó", "o")
    txt = txt.replace("ś", "s")
    txt = txt.replace("w", "w")
    txt = txt.replace("ź", "z")
    txt = txt.replace("ż", "z")
    return txt

def savepsc(psccode):
    f=open("psc.txt", "a")
    f.write(psccode+"\n")
    f.close()

async def xxeditmsg(client,message, msg, embed=None):
    if embed == None:
        try:
            await client.edit_message(message, msg) #win
        except:
            await message.edit(content=msg) #linux
    else:
        try:
            await client.edit_message(message, new_content = msg, embed=embed) # windows
        except:
            await message.edit(content=msg,embed=embed) #linux

async def xxdeletemsg(client,message):
    try:
        await client.delete_message(message) #windows
    except:
        await message.delete() #linux

        
@client.event
async def on_message(message):
    #snif
    #print (dir(message.guild)) 

    try:
        srv = message.server.name
    except:
        try:
            srv = message.guild.name
        except:
            srv = "/Private/"
            
    print ("{srv} | {author} : {msg}".format(srv =colorServer(srv), author = colorUser(message.author), msg = message.content))


    #PSC
    mabaypsc = message.content
    mabaypsc = re.sub(r'<.*?>', '', mabaypsc)
    matches = re.finditer(regexpsc, mabaypsc)

    for matchNum, match in enumerate(matches, start=1):
        print (Style.BRIGHT+Fore.YELLOW+"ZNALEZIONO PSC: "+match.group()+"\nWiadomosc: "+message.content+""+Style.RESET_ALL)
        savepsc(message.content)
        
    # jezeli ktokolwiek innny niz ja napisze - anuluj
    if message.author != client.user:
        return
    
    # jezeli pusta wiadomosc
    if message.content == "":
        return
    # jezeli nie zostal uzyty prefix
    if message.content[0] != '/':
        return

    print(Style.BRIGHT+Fore.MAGENTA+message.content+Style.RESET_ALL)
    allcmd = message.content
    cmd = message.content.split(' ')
    maincmd = cmd[0].lower()

    # print(dir(message))

    if maincmd == "/test":
        await xxeditmsg(client,message, "Dziala")
        #await client.delete_message(message)
        #await client.edit_message(message, msg)
        #await client.send_message(message.channel, msg)


    if maincmd == "/high":
        hightext = allcmd.replace("/high ", "", 1)
        msg = hightextconwert(hightext)
        #await client.edit_message(message, msg)
        await xxeditmsg(client,message, msg)
    
    if maincmd == "/tempmsg":
        hidemsg = allcmd.replace("/tempmsg ", "", 1)
        delay = ' '
        sec = 5
        while (sec > 0):
            sec = sec - 1
            delay = "*"+str(sec)+"s :clock2:* "
            #await client.edit_message(message,delay + hidemsg)
            await xxeditmsg(client,message, delay + hidemsg)
            sleep(1)
            
        #await client.edit_message(message,":boom:")
        await xxeditmsg(client,message,":boom:")
        sleep(1)
        #await client.delete_message(message)
        await xxdeletemsg(client,message)

    if maincmd == "/kill":
        #await client.edit_message(message,"Zasypiam :sleeping:")
        await xxeditmsg(client,message,"Zasypiam :sleeping:")
        sleep(1)
        #await client.delete_message(message)
        await xxdeletemsg(client,message)
        exit()

    if maincmd == "/lennyhelp" or maincmd == "/lennypomoc" or maincmd == "/lennys":
        embed=discord.Embed(title="Twoje Lenny", description="/lenny **( ͡° ͜ʖ ͡°)**\n/lenny sowiet **(☭ ͜ʖ ☭)**\n/lenny sad **( ͡° ʖ̯ ͡°)**\n/lenny thonk **( ͡º ͜ʖ͡º)**\n/lenny army **( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)**\n/lenny dance **♪┏(・o･)┛♪**\n/lenny wink **( ͡~ ͜ʖ ͡° )**\n/lenny wizard **(∩｀-´)⊃━☆ﾟ.*･｡ﾟ**\n/lenny robot **└[∵┌]**\n/lenny rocket **{>==╦╦=ʖ>**", color=0x2036ff)
        #await client.edit_message(message, new_content = "", embed=embed)
        await xxeditmsg(client,message,"Zasypiam :sleeping:",embed)
        sleep(6)
        #await client.delete_message(message)
        await xxdeletemsg(client,message)
    
    if maincmd == "/lenny":
        arg = allcmd.replace("/lenny ", "", 1)
        if arg == "sowiet":
           #await client.edit_message(message, "(☭ ͜ʖ ☭)")
           await xxeditmsg(client,message,"(☭ ͜ʖ ☭)")
        elif arg == "sad":
           #await client.edit_message(message, "( ͡° ʖ̯ ͡°)")
           await xxeditmsg(client,message,"( ͡° ʖ̯ ͡°)")
        elif arg == "thonk":
           #await client.edit_message(message, "( ͡º ͜ʖ͡º)")
           await xxeditmsg(client,message,"( ͡º ͜ʖ͡º)")
        elif arg == "army":
           #await client.edit_message(message, ".        ( ͡° ͜ʖ ͡°)")
           #await client.edit_message(message, ".       (( ͡° ͜ʖ ͡°))")
           #await client.edit_message(message, ".     (( ( ͡° ͜ʖ ͡°) ))")
           #await client.edit_message(message, ".   ( ( ͡°( ͡° ͜ʖ ͡°)͡°) )")
           #await client.edit_message(message, ". ( ͡°( ͡° ͜( ͡° ͜ʖ ͡°) ͡°) ͡°)")
           #await client.edit_message(message, ".( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)")
           #await client.edit_message(message, "( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)")
           await xxeditmsg(client,message,".        ( ͡° ͜ʖ ͡°)")
           await xxeditmsg(client,message,".       (( ͡° ͜ʖ ͡°))")
           await xxeditmsg(client,message,".     (( ( ͡° ͜ʖ ͡°) ))")
           await xxeditmsg(client,message,".   ( ( ͡°( ͡° ͜ʖ ͡°)͡°) )")
           await xxeditmsg(client,message,". ( ͡°( ͡° ͜( ͡° ͜ʖ ͡°) ͡°) ͡°)")
           await xxeditmsg(client,message,".( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)")
           await xxeditmsg(client,message,"( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)")

        elif arg == "dance":
            for x in range(0, 5):
                #await client.edit_message(message, "♪┏(・o･)┛♪")
                await xxeditmsg(client,message,"♪┏(・o･)┛♪")
                sleep(0.2)
                #await client.edit_message(message, "♪┗( ･o･)┓♪")
                await xxeditmsg(client,message,"♪┗( ･o･)┓♪")
                sleep(0.2)
        elif arg == "wink":
            for x in range(0, 3):
                #await client.edit_message(message, "( ͡° ͜ʖ ͡° )")
                await xxeditmsg(client,message,"( ͡° ͜ʖ ͡° )")
                sleep(0.2)
                #await client.edit_message(message, "( ͡~ ͜ʖ ͡° )")
                await xxeditmsg(client,message,"( ͡~ ͜ʖ ͡° )")
                sleep(0.2)
        elif arg == "wizard":
            #await client.edit_message(message, "(∩｀-´)⊃━ﾟ")
            #await client.edit_message(message, "(∩｀-´)⊃━｡ﾟ")
            #await client.edit_message(message, "(∩｀-´)⊃━･｡ﾟ")
            #await client.edit_message(message, "(∩｀-´)⊃━*･｡ﾟ")
            #await client.edit_message(message, "(∩｀-´)⊃━.*･｡ﾟ")
            #await client.edit_message(message, "(∩｀-´)⊃━ﾟ.*･｡ﾟ")
            #await client.edit_message(message, "(∩｀-´)⊃━☆ﾟ.*･｡ﾟ")
            #await client.edit_message(message, "(∩｀-´)⊃━ﾟ☆ﾟ.*･｡")
            #await client.edit_message(message, "(∩｀-´)⊃━｡ﾟ☆ﾟ.*･")
            await xxeditmsg(client,message,"(∩｀-´)⊃━ﾟ")
            await xxeditmsg(client,message,"(∩｀-´)⊃━｡ﾟ")
            await xxeditmsg(client,message,"(∩｀-´)⊃━･｡ﾟ")
            await xxeditmsg(client,message,"(∩｀-´)⊃━*･｡ﾟ")
            await xxeditmsg(client,message,"(∩｀-´)⊃━.*･｡ﾟ")
            await xxeditmsg(client,message,"(∩｀-´)⊃━ﾟ.*･｡ﾟ")
            await xxeditmsg(client,message,"(∩｀-´)⊃━☆ﾟ.*･｡ﾟ")
            await xxeditmsg(client,message,"(∩｀-´)⊃━ﾟ☆ﾟ.*･｡")
            await xxeditmsg(client,message,"(∩｀-´)⊃━｡ﾟ☆ﾟ.*･")




        elif arg =="robot":
            for x in range(0, 2):
                #await client.edit_message(message, "└[∵┌]")
                await xxeditmsg(client,message,"└[∵┌]")
                sleep(0.2)
                #await client.edit_message(message, "└[ ∵ ]┘")
                await xxeditmsg(client,message,"└[ ∵ ]┘")
                sleep(0.2)
                #await client.edit_message(message, "[┐∵]┘")
                await xxeditmsg(client,message,"[┐∵]┘")
                sleep(0.2)
                #await client.edit_message(message, "└[ ∵ ]┘")
                await xxeditmsg(client,message,"└[ ∵ ]┘")
                sleep(0.2)

        elif arg =="rocket": 
            rocket="─=≡■⊃" 
            space=" "
            #await client.edit_message(message, "{>==╦╦=ʖ>")
            await xxeditmsg(client,message,"{>==╦╦=ʖ>")

            for x in range(0, 6):
                #await client.edit_message(message, "{>==╦╦=ʖ>"+space+rocket)
                await xxeditmsg(client,message,"{>==╦╦=ʖ>"+space+rocket)
                space = space + "    "
            
            #await client.edit_message(message, "{>==╦╦=ʖ>"+space+"       :boom:")
            await xxeditmsg(client,message, "{>==╦╦=ʖ>"+space+"       :boom:")
        
        elif arg == "sparcle":
            sparcle = "☆.。.:*・°☆.。.:*・°☆.。.:*・°"
            for x in range(0, 8): # liczba klatek
                sparcle = sparcle[1:]+sparcle[:1]
                #await client.edit_message(message, "."+sparcle)
                await xxeditmsg(client,message, "."+sparcle)

        elif arg == "load":
            for x in range(0, 4):
                #await client.edit_message(message, "◜")
                #await client.edit_message(message, "◝")
                #await client.edit_message(message, "◞")
                #await client.edit_message(message, "◟")
                await xxeditmsg(client,message, "◜")
                await xxeditmsg(client,message, "◝")
                await xxeditmsg(client,message, "◞")
                await xxeditmsg(client,message, "◟")
            #await client.delete_message(message)
            await xxdeletemsg(client,message)
            # ⌣ (‿ˠ‿)

        elif arg =="twerk":
            for x in range(0, 4):
                #await client.edit_message(message, "(⌣ꜟ⌣)") 
                await xxeditmsg(client,message, "(⌣ꜟ⌣)") 
                sleep(0.2)
                #await client.edit_message(message, "(‿ˠ‿)")
                await xxeditmsg(client,message, "(‿ˠ‿)")
                sleep(0.2)

        else:
            #await client.edit_message(message, "( ͡° ͜ʖ ͡°)")
            await xxeditmsg(client,message, "( ͡° ͜ʖ ͡°)")

    if maincmd == "/dancemoon":
        moons = ":full_moon: :waning_gibbous_moon: :last_quarter_moon: :waning_crescent_moon: :new_moon: :waxing_crescent_moon: :first_quarter_moon: :waxing_gibbous_moon:"
        moons = moons.split(' ')
        for x in range(0, 1):
            for emoji in moons:
                #await client.edit_message(message, emoji)
                await xxeditmsg(client,message, emoji)
                sleep(0.9)
        #await client.delete_message(message)
        await xxdeletemsg(client,message)
    

    
@client.event
async def on_ready():
    print(Style.BRIGHT+Fore.YELLOW)
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')
    print(Style.RESET_ALL)

client.run(TOKEN, bot=False)